﻿/** @jsx React.DOM */

// Create a custom component by calling React.createClass.

var TimerExample = React.createClass({

    getInitialState: function(){

        // This is called before our render function. The object that is 
        // returned is assigned to this.state, so we can use it later.

        return { elapsed: 0 };
    },

    componentDidMount: function(){

        // componentDidMount is called by react when the component 
        // has been rendered on the page. We can set the interval here:

        this.timer = setInterval(this.tick, 50);
    },

    componentWillUnmount: function(){

        // This method is called immediately before the component is removed
        // from the page and destroyed. We can clear the interval here:

        clearInterval(this.timer);
    },

    tick: function(){

        // This function is called every 50 ms. It updates the 
        // elapsed counter. Calling setState causes the component to be re-rendered

        this.setState({elapsed: new Date() - this.props.start});
    },

    render: function() {
        var datetime = new Date();
		var currenttime = datetime.toLocaleDateString()+" "+ datetime.toLocaleTimeString();
        return <p><b>{currenttime}</b></p>;
    }
});


React.render(
    <TimerExample start={Date.now()} />,
    document.getElementById('DateTime')
);